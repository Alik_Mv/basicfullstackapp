module.exports = api => {
  api.cache(true);

  return {
    ignore: [/(node_module)/],
    presets: [
      [
        "@babel/preset-env",
        {
          useBuiltIns: "usage",
          corejs: {
            version: 3,
          },
          targets: {
            esmodules: true,
          },      
        }
      ],
      "@babel/preset-react",
    ],
    plugins: [
      [
        "@babel/plugin-transform-runtime",
      ],
      "@babel/plugin-transform-react-constant-elements",
    ]
  }
}