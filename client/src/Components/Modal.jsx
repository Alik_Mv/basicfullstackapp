import React from "react";

const Modal = ({ showModal }) => {
  const getHeader = () => {
    return (
      <div className="modal-header">
        <h5 className="modal-title">{"Modal title"}</h5>
        <button
          type="button"
          className="close"
          aria-label="Close"
          onClick={() => showModal(false)}
        >
          <span>&times;</span>
        </button>
      </div>
    );
  };

  const getBody = () => {
    return (
      <div className="modal-body">
        <span>{"Some text in modal."}</span>
      </div>
    );
  };

  const getFooter = () => {
    return (
      <div className="modal-footer">
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => showModal(false)}
        >
          {"Close"}
        </button>
      </div>
    );
  };

  return (
    <div className="modal-dialog modal-dialog-centered">
      <div className="modal-content">
        {getHeader()}
        {getBody()}
        {getFooter()}
      </div>
    </div>
  );
};

export default Modal;
