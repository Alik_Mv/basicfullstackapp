import React, { useState } from "react";
import logo from "../media/react.svg";
import Modal from "./Components/Modal";
import { useHttp } from "./Hooks/http.hook";

const App = () => {
  const [isVisible, setIsVisible] = useState(false);
  const { isLoading, request, error } = useHttp();

  const requestHandler = async (msg) => {
    try {
      const data = await request("/api/hello/", "POST", { message: msg });
    } catch (e) {
      console.log(e);
    }
  };

  return isVisible ? (
    <>
      <Modal showModal={setIsVisible} />
      <div className="d-flex justify-content-center">
        {isLoading ? (
          <div class="spinner-border text-primary" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        ) : (
          <span>{"I hear the server in console!"}</span>
        )}
      </div>
    </>
  ) : (
    <>
      <div className="d-flex justify-content-center">
        <h1 className="display-4">{"React app working"}</h1>
      </div>
      <div className="d-flex flex-column">
        <div className="d-flex justify-content-center">
          <img
            className="rounded mx-auto d-block"
            style={{ width: "300px" }}
            src={logo}
          />
        </div>
        <div className="d-flex justify-content-center">
          <button
            type="button"
            className="btn btn-outline-primary btn-mm"
            data-toggle="modal"
            data-target="#exampleModal"
            onClick={() => {
              setIsVisible((prev) => !prev);
              requestHandler("Message from client button");
            }}
          >
            {"CLICK"}
          </button>
        </div>
      </div>
    </>
  );
};

export default App;
