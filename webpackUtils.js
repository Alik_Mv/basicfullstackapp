const HTMLWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const WebpackShellPluginNext = require('webpack-shell-plugin-next');

const clientPlugins = (isDev, templatePath, faviconPath, buildPath) => {
  const allPlugins = [
    new HTMLWebpackPlugin({
      template: templatePath,
      collapseWhitespace: !isDev,
      cache: false,
    }),
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: faviconPath,
          to: buildPath,
        },
      ],
    }),
    new MiniCssExtractPlugin({
      filename: filename(isDev, "css"),
    }),
  ];

  return allPlugins;
};

const serverPlugins = () => {
  const allPlugins = [
    new CleanWebpackPlugin(),
    new WebpackShellPluginNext({
      onBuildExit:{
        scripts: [
          'nodemon ./build/server/server.js --watch build/server',
          'open-cli http://localhost:5000'
        ],
        blocking: false,
        parallel: true
      }
    }),
  ];

  return allPlugins;
};

const clientOptimization = (isDev) => ({
  minimize: !isDev,
  minimizer: [
    new TerserPlugin(),
    new CssMinimizerPlugin({
      test: /\.css$/i,
    }),
  ],
});

const serverOptimization = (isDev) => ({
  minimize: !isDev,
  minimizer: [new TerserPlugin()],
});

const filename = (isDev, ext) => {
  return `[name].${isDev ? ext : `[hash].${ext}`}`;
};

const defaultCssLoaders = (isDev) => {
  return [
    {
      loader: MiniCssExtractPlugin.loader,
    },
    {
      loader: "css-loader",
      options: {
        sourceMap: !isDev,
      },
    },
    {
      loader: "postcss-loader",
      options: {
        sourceMap: !isDev,
      },
    },
  ];
};

const sassLoader = (isDev) => {
  return {
    loader: "sass-loader",
    options: {
      implementation: require("sass"),
      sourceMap: !isDev,
      sassOptions: {
        fiber: require("fibers"),
      },
    },
  };
};

const mode = (isDev) => ({
  mode: isDev ? "development" : "production",
});

module.exports = {
  clientPlugins,
  serverPlugins,
  clientOptimization,
  serverOptimization,
  filename,
  defaultCssLoaders,
  sassLoader,
  mode,
};
