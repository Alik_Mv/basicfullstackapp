import 'ignore'
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import express from 'express';
import config from 'config';
import path from "path";
import fs from 'fs';
import App from '../client/src/App';
import { helloRoute } from './Routes/hello';

const app = express();
const __dirname = path.resolve();
const { port } = config || 5000;

app.use(express.static(path.resolve(__dirname, 'build/client')));

app.use(express.json({ extended: true }))

app.get("/", (req, res) => {
  const app = ReactDOMServer.renderToString(<App />);
  const indexFile = path.resolve(__dirname, 'build/client/index.html');
  fs.readFile(indexFile, "utf8", (err, data) => {
    res.setHeader('Content-Type', 'application/json');
    if (err) {
      console.error("Something went wrong:", err);
      return res
        .status(500)
        .send("Something error happened. Try go to home page or visit us later!");
    }

    return res.send(
      data.replace('<div id="root"></div>', `<div id="root">${app}</div>`)
    );
  });
  res.sendFile(path.resolve(__dirname, 'build/client/index.html'));
});

app.use("/api", helloRoute);

app.listen(port, () => console.log(`App started at port ${port}`));
