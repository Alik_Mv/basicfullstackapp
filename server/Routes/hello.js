import { Router } from "express";

const router = Router();

router.post("/hello", async (req, res) => {
  try {
    setTimeout(() => {
      res.status(200).json({
        message: `Hello from server! I did get this: ${req.body.message}`,
      });
    }, 1000);
  } catch (e) {
    console.log(e);
    res.status(500).json({ message: "Something went wrong on server" });
  }
});

export { router as helloRoute };
