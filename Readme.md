# Example of the react basic fullstack application.

Used technologies:

- Webpack
- Babel
- Express
- React
- Bootstrap

# Quick start:

Just write in console next commands to start application:
>npm i && npm start

# Build:

For build app in development mode you need run next command in console:
> npm run dev

and to build in production mode run:

> npm run build

The difference between them is in the source code minifications. In the development mode your code will be more readable then in production mode. This will make debugging easier and more convenient.

Example client.js in **development** mode:

```javascript
var _ref2 = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h5", {
  className: "modal-title"
}, "Modal title");
```

Example client.js in **production** mode:
```javascript
(()=>{"use strict";var e,r,t,n,o,a,i,c={4767:(e,r,t)=>{var n=t(7294),o=t(3935);t(6992),t(3948);const a=t.p+"e68b35f4774979ae220b1d5d7b345b00.svg";var i=n.createElement("h5",{className:"modal-title"},"Modal title")...
```

# Extra information

If you want to start only client without server you can use this command:
>npm run hotClient

it will start **webpack-dev-server** with *--hot* argument set to true(which setted in webpack.config.js).

<br />
<hr>
This project will be updated time to time.
<br />
Authored: Albert Miraev.
