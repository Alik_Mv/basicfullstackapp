const path = require("path");
const {
  clientPlugins,
  clientOptimization,
  filename,
  defaultCssLoaders,
  sassLoader,
  mode,
} = require("../../webpackUtils");

const isDev = process.env.NODE_ENV === "development";

const buildPath = path.resolve(__dirname, "../../build/client");
const faviconPath = path.resolve(__dirname, "../../client/media/favicon.ico");
const templatePath = path.resolve(__dirname, "../../client/index.html");
const entryPointPath = path.resolve(__dirname, "../../client/index.jsx");
const bootstrapPath = path.resolve(__dirname, "../../node_modules/bootstrap");

module.exports = () => ({
  name: "client",
  ...mode(),
  entry: {
    client: entryPointPath,
  },
  output: {
    publicPath: "/",
    filename: filename(isDev, "js"),
    path: buildPath,
  },
  resolve: {
    extensions: [".js", ".jsx"],
  },
  plugins: clientPlugins(isDev, templatePath, faviconPath, buildPath),
  module: {
    rules: [
      {
        test: /\.css$/,
        use: defaultCssLoaders(isDev),
      },
      {
        test: /\.s[ac]ss$/,
        exclude: [bootstrapPath],
        use: [...defaultCssLoaders(isDev), sassLoader(isDev)],
      },
      {
        test: /\.(png|jpg|jpeg|svg|gif)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: filename(isDev, '[ext]'),
          },
        },
      },
      {
        test: /\.m?jsx?$/,
        exclude: [/core-js/, /node_modules/, /regenerator-runtime/],
        use: {
          loader: "babel-loader",
        },
      },
    ],
  },
  devtool: isDev && "source-map",
  optimization: {
    splitChunks: {
      chunks: "all",
    },
    ...clientOptimization(isDev),
  },
  ...(isDev && {devServer: {
    publicPath: '/',
    contentBase: "./build/client",
    port: 8000,
    hot: true,
    open: true,
  }}),
});
