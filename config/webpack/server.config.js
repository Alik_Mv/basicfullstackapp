const path = require("path");
const nodeExternals = require("webpack-node-externals");
const {
  serverPlugins,
  serverOptimization,
  filename,
  mode,
} = require("../../webpackUtils");

const isDev = process.env.NODE_ENV === "development";

const entryPointPath = path.resolve(__dirname, "../../server/server.js");
const buildPath = path.resolve(__dirname, "../../build/server");

module.exports = () => {
  return {
    name: "server",
    ...mode(),
    entry: {
      server: entryPointPath,
    },
    target: "node",
    externals: [nodeExternals()],
    output: {
      path: buildPath,
      filename: filename(isDev, "js"),
    },
    resolve: {
      extensions: [".js", ".jsx"],
    },
    plugins: serverPlugins(),
    module: {
      rules: [
        {
          test: /\.(png|jpg|jpeg|svg|gif)$/,
          use: {
            loader: 'file-loader',
            options: {
              name: filename(isDev, '[ext]'),
            },
          },
        },
        {
          test: /\.m?jsx?$/,
          exclude: [/core-js/, /node_modules/, /regenerator-runtime/],
          use: {
            loader: "babel-loader",
          },
          resolve: {
            fullySpecified: false
          }
        },
      ],
    },
    optimization: serverOptimization(isDev),
  };
};
